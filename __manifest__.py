# -*- coding: utf-8 -*-
{
    'name': "Customer FHIR",

    'summary': """
        Integration Customer module with FHIR""",

    'description': """
        Integraton customer module with FHIR for traceability.
    """,

    'author': "eHealth ID",
    'website': "https://ehealth.id",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base', 'fhir_config'],

    'data': [
        'views/customer_fhir.xml'
    ],
}
