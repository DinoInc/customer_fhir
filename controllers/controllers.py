
# -*- coding: utf-8 -*-

import requests
import json
import logging
import re

from fhirclient.models.patient import Patient
from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)

class Doctor(http.Controller):
    def getIdFromResponse(self, resource, response):
        diagnostics = response['issue'][0]['diagnostics']
        match = re.search('(' + resource + '/\d+)/_history/\d+', diagnostics)
        resourceId = match.group(1)
        return resourceId

    def getRepresentativeName(self, patient):
        officialName = list(filter(lambda it: ('use' in it) and (it['use'] == 'official'), patient['name']));
        if len(officialName) > 0:
            return officialName[0]

        if len(patient['name']) > 0:
            return patient['name'][0]

        return { 'family': '[no name]' }

    def getNameFromPatient(self, patient):
        name = self.getRepresentativeName(patient)
        
        if 'given' in name:
            return ' '.join(name['given']) + ' ' + name['family']

        return name['family']
    

    def getRepresentativeAddress(self, patient):
        homeAddress = list(filter(lambda it: ('use' in it) and (it['use'] == 'home'), patient['address']));
        if len(homeAddress) > 0:
            return homeAddress[0]

        if len(patient['address']) > 0:
            return patient['address'][0]

        return { 'line': ['[no address]'] }

    def getAddressStreetFromPatient(self, patient):
        address = self.getRepresentativeAddress(patient)
        return "\n".join(address['line'])

    def getAddressCityFromPatient(self, patient):
        address = self.getRepresentativeAddress(patient)
        return address['city']

    @http.route('/doctor/doctor/', type = 'json', auth='public', method=['POST'])
    def index(self, **kw):

        client = request.env['fhir_config.config'].sudo().getClientInstance()
        patientInstance = Patient(kw)
        response = Patient.create(patientInstance, client.server)
        patientId = self.getIdFromResponse('Patient', response)

        _logger.info(kw)

        customer = {
            "patientId": patientId,
            "name": self.getNameFromPatient(kw),
            "street": self.getAddressStreetFromPatient(kw),
            "city": self.getAddressCityFromPatient(kw),
        }

        record = request.env['res.partner'].sudo().create(customer)

        _logger.info(response)

        return record

#     @http.route('/doctor/doctor/objects/<model('doctor.doctor'):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('doctor.object', {
#             'object': obj
#         })