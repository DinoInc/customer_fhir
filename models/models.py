# -*- coding: utf-8 -*-

from odoo import models, fields, api

class customer_fhir(models.Model):
  _inherit = 'res.partner'  

  patientId = fields.Char(string="Patient Id", readonly = True)
          
